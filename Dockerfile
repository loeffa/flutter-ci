# based on https://medium.com/@chima_37359/setup-gitlab-ci-in-flutter-android-project-89f6628828e8

FROM openjdk:8-jdk

RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes --no-install-recommends \
    wget tar unzip lib32stdc++6 lib32z1 \
    build-essential

ENV ANDROID_COMPILE_SDK="29"
ENV ANDROID_BUILD_TOOLS="29.0.2"
ENV ANDROID_SDK_TOOLS="4333796"
ENV FLUTTER_VERSION="https://storage.googleapis.com/flutter_infra/releases/stable/linux/flutter_linux_v1.12.13+hotfix.8-stable.tar.xz"

RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip
RUN unzip -d android-sdk-linux android-sdk.zip
RUN echo y | android-sdk-linux/tools/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null
RUN echo y | android-sdk-linux/tools/bin/sdkmanager "platform-tools" >/dev/null
RUN echo y | android-sdk-linux/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null
ENV ANDROID_HOME=$PWD/android-sdk-linux

ENV PATH="${PATH}:$ANDROID_HOME/tools"
ENV PATH="${PATH}:$ANDROID_HOME/platform-tools"
ENV PATH="${PATH}:$ANDROID_HOME/build-tools/23.0.1"
RUN yes | android-sdk-linux/tools/bin/sdkmanager --licenses

# flutter sdk setup
RUN wget --output-document=flutter-sdk.tar.xz $FLUTTER_VERSION
RUN tar -xf flutter-sdk.tar.xz
ENV PATH="${PATH}:$PWD/flutter/bin"

# fastlane setup
# RUN gem install fastlane
# RUN fastlane update_fastlane

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
